import React, { Component } from "react";
import CloseIcon from "@mui/icons-material/Close";
import styled from "@emotion/styled";
import { Button } from "@mui/material";
import Menu from "@mui/material/Menu";
import AssignmentTurnedInIcon from "@mui/icons-material/AssignmentTurnedIn";
import CreditScoreIcon from "@mui/icons-material/CreditScore";
import { Dialog, DialogTitle } from "@mui/material";
import { Stack, DialogContent, DialogActions } from "@mui/material";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { Box } from "@mui/system";
import { OutlinedInput } from "@mui/material";
import MenuItem from "@mui/material/MenuItem";

import { TextField, Typography } from "@mui/material";
import { getCards, createCards, deleteCard, getChecklist } from "./Redux/store";
import { deleteChecklist, createChecklist } from "./Redux/store";
import { deleteChecklistItem, createsNewCheckItems } from "./Redux/store";
import { connect } from "react-redux";

const ListBox = styled(Box)({
  background: "white",
  padding: "5px",
  color: "black",
  marginBottom: "10px",
  borderRadius: "2px",
  boxShadow: "1px 1px 4px gray",
});
class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newCardName: "",
      addacard: false,
      add: false,
      open: false,
      openChecklist: false,
      openChecklistAdd: false,
      openChecklistItem: false,
      error: false,
      cardName: "",
      cardsID: "",
      createChecklistName: "",
      ChecklistName: "",
      ChecklistID: "",
      createChecklistItemName: "",
    };
  }

  handelOpen = () => {
    this.setState({
      addacard: true,
    });
  };

  openChecklistAdd = () => {
    this.setState({
      openChecklistAdd: true,
      openChecklistItem: false,
    });
  };
  openChecklistItem = () => {
    this.setState({
      openChecklistItem: true,
      openChecklistAdd: false,
    });
  };
  CloseChecklistAdd = () => {
    this.setState({
      openChecklistAdd: false,
    });
  };

  closeChecklistItem = () => {
    this.setState({
      openChecklistItem: false,
      ChecklistName: "",
    });
  };
  handleClose = () => {
    this.setState({
      addacard: false,
      add: false,
      open: false,
      openChecklist: false,
      cardsID: "",
      cardName: "",
    });
  };

  newCardName = (event) => {
    this.setState({
      newCardName: event.target.value,
    });
  };
  createChecklistName = (event) => {
    this.setState({
      createChecklistName: event.target.value,
    });
  };
  createChecklistItemName = (event) => {
    this.setState({
      createChecklistItemName: event.target.value,
    });
  };

  HandleCreatecard = () => {
    this.props.createCards(this.props.listid, this.state.newCardName);
    this.setState({
      // addacard: false,
      newCardName: "",
    });
  };
  createChecklist = () => {
    this.props.createChecklist(
      this.state.createChecklistName,
      this.state.cardsID
    );
    this.setState({
      openChecklistAdd: false,

      createChecklistName: "",
    });
  };
  getsChecklist = (cardsID) => {
    this.props.getChecklist(cardsID);
  };
  createsNewCheckItems = () => {
    this.props.createsNewCheckItems(
      this.state.ChecklistID,
      this.state.createChecklistItemName
    );
    this.setState({
      openChecklistItem: false,
    });
  };
  openChecklist = () => {
    this.setState({
      openChecklist: true,
    });
  };

  deleteChecklistItem = (ChecklistID, checklistItemId) => {
    this.props.deleteChecklistItem(ChecklistID, checklistItemId);
    this.props.getChecklist();
  };

  componentDidMount() {
    this.props.getCards(this.props.listid);
  }
  render() {
    return (
      <>
        {this.props.store.Cards ? (
          <>
            {this.props.store.Cards.filter((card) => {
              return card.idList == this.props.listid;
            }).map((cards, index) => {
              return (
                <>
                  <ListBox key={`L${index}`}>
                    <div
                      style={{
                        display: "flex ",
                        justifyContent: "space-between",
                        cursor: "pointer",
                      }}
                      className=""
                      key={cards.id}
                    >
                      <Typography
                        key={`T${index}`}
                        id={cards.id}
                        onClick={(e) => {
                          this.setState({
                            cardName: cards.name,
                            cardsID: cards.id,
                          });
                          this.openChecklist(e);
                          this.getsChecklist(cards.id);
                        }}
                        width={225}
                      >
                        {cards.name}
                      </Typography>
                      <DeleteForeverIcon
                        key={`d${index}`}
                        style={{ cursor: "pointer" }}
                        className="hover:text-red-700"
                        onClick={() => {
                          this.props.deleteCard(cards.id);
                        }}
                      ></DeleteForeverIcon>
                    </div>
                  </ListBox>
                  <Stack>
                    <Dialog
                      open={this.state.openChecklist}
                      onClose={this.handleClose}
                    >
                      <DialogTitle style={{ width: "552px", height: "" }}>
                        <CreditScoreIcon style={{ marginRight: "20px" }} />
                        {this.state.cardName}
                      </DialogTitle>
                      <hr />
                      {this.props.store.checkList ? (
                        this.props.store.checkList
                          .filter((fil) => {
                            return fil.idCard == this.state.cardsID;
                          })
                          .map((checklist, index) => {
                            return (
                              <>
                                <ListBox
                                  key={`LL${index}`}
                                  style={{ margin: "10px" }}
                                >
                                  <div
                                    style={{
                                      display: "flex ",
                                      justifyContent: "space-between",
                                    }}
                                    className=""
                                    key={cards.id}
                                  >
                                    <Typography
                                      key={`TT${index}`}
                                      id={cards.id}
                                      style={{
                                        width: "225px",
                                        fontSize: "16px",
                                        fontWeight: "bold",
                                      }}
                                    >
                                      {checklist.name}
                                    </Typography>
                                    <DeleteForeverIcon
                                      key={`DD${index}`}
                                      style={{ cursor: "pointer" }}
                                      className="hover:text-red-700"
                                      onClick={() => {
                                        this.props.deleteChecklist(
                                          checklist.id
                                        );
                                        // this.getsCheckItems(checklist.id);
                                      }}
                                    ></DeleteForeverIcon>
                                  </div>
                                  <hr />
                                  <div>
                                    {checklist.checkItems.map(
                                      (checkItem, index) => {
                                        return (
                                          <div
                                            key={`DDD${index}`}
                                            className="flex items-center "
                                          >
                                            <Typography
                                              key={`TTT${index}`}
                                              style={{
                                                width: "500px",
                                              }}
                                            >
                                              {checkItem.name}
                                            </Typography>

                                            <DeleteForeverIcon
                                              key={`DDD${index}`}
                                              className=" cursor-pointer hover:text-red-700"
                                              onClick={() => {
                                                this.deleteChecklistItem(
                                                  checklist.id,
                                                  checkItem.id
                                                );
                                              }}
                                            />
                                          </div>
                                        );
                                      }
                                    )}
                                  </div>
                                </ListBox>
                                <Button
                                  variant="outlined"
                                  style={{
                                    width: "140px",
                                    marginLeft: "10px",
                                    display: "flex",
                                    float: "right",
                                  }}
                                  onClick={(e) => {
                                    this.setState({
                                      ChecklistName: checklist.name,
                                      ChecklistID: checklist.id,
                                    });

                                    this.openChecklistItem(e);
                                  }}
                                >
                                  Add an item
                                </Button>
                              </>
                            );
                          })
                      ) : (
                        <></>
                      )}

                      <DialogActions>
                        <Button
                          style={{
                            width: "125px",
                          }}
                          variant="contained"
                          onClick={this.openChecklistAdd}
                        >
                          <AssignmentTurnedInIcon />
                          checklist
                        </Button>
                        <Button onClick={this.handleClose}>Cancel</Button>
                      </DialogActions>
                    </Dialog>
                  </Stack>
                  <div className="px-10">
                    <Menu
                      id="basic-menu"
                      open={this.state.openChecklistAdd}
                      onClose={this.state.handleClose}
                      MenuListProps={{
                        "aria-labelledby": "basic-button",
                      }}
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        position: "absolute",
                        left: "50%",
                        transform: "translate(15%,-50%)",
                      }}
                    >
                      <div className="flex w-[325px] justify-between">
                        <Typography paddingLeft={15}>Add checklist</Typography>{" "}
                        <MenuItem>
                          <CloseIcon
                            onClick={this.CloseChecklistAdd}
                            className="h-2 float-right pb-2"
                          />
                        </MenuItem>
                      </div>
                      <hr />
                      <Typography
                        style={{ fontWeight: "bold", paddingLeft: "20px" }}
                      >
                        Tilte
                      </Typography>
                      <OutlinedInput
                        onChange={this.createChecklistName}
                        className="w-[90%] ml-4 h-10 text-xl"
                      />
                      <Button
                        style={{
                          fontWeight: "bold",
                          marginLeft: "20px",
                          marginTop: "6px",
                        }}
                        variant="contained"
                        size="small"
                        onClick={this.createChecklist}
                      >
                        Add
                      </Button>
                    </Menu>
                  </div>
                  <div className="px-10">
                    <Menu
                      id="basic-menu"
                      open={this.state.openChecklistItem}
                      onClose={this.state.handleClose}
                      MenuListProps={{
                        "aria-labelledby": "basic-button",
                      }}
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        position: "absolute",
                        left: "50%",
                        transform: "translate(15%,-50%)",
                      }}
                    >
                      <div className="flex w-[325px] justify-between">
                        <Typography paddingLeft={15}>
                          {this.state.ChecklistName}
                        </Typography>
                        <MenuItem>
                          <CloseIcon
                            onClick={this.closeChecklistItem}
                            className="h-2 float-right pb-2"
                          />
                        </MenuItem>
                      </div>
                      <hr />
                      <Typography
                        style={{ fontWeight: "bold", paddingLeft: "20px" }}
                      >
                        Add an Item
                      </Typography>
                      <OutlinedInput
                        onChange={this.createChecklistItemName}
                        className="w-[90%] ml-4 h-10 text-xl"
                      />
                      <Button
                        style={{
                          fontWeight: "bold",
                          marginLeft: "20px",
                          marginTop: "6px",
                        }}
                        variant="contained"
                        size="small"
                        onClick={this.createsNewCheckItems}
                      >
                        Add
                      </Button>
                    </Menu>
                  </div>
                </>
              );
            })}
          </>
        ) : (
          "Loading....."
        )}
        {this.state.addacard ? (
          <>
            <TextField
              id="outlined-basic"
              size="small"
              fullWidth
              label="Enter a title for this card…"
              type={"textarea"}
              variant="outlined"
              onChange={this.newCardName}
              value={this.state.newCardName}
            />
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: "8px",
              }}
            >
              <Button
                variant="contained"
                size="small"
                onClick={this.HandleCreatecard}
              >
                Add Card
              </Button>
              <CloseIcon
                onClick={this.handleClose}
                className="m-2 cursor-pointer"
              ></CloseIcon>
            </div>
          </>
        ) : (
          <Button
            variant="text"
            size="small"
            onClick={this.handelOpen}
            className="text-secondary"
          >
            + Add a Card
          </Button>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    store: state,
  };
};

const mapDispatchToProps = {
  getCards: getCards,
  createCards: createCards,
  deleteCard: deleteCard,
  getChecklist: getChecklist,
  deleteChecklist: deleteChecklist,
  createChecklist: createChecklist,
  createsNewCheckItems: createsNewCheckItems,
  deleteChecklistItem: deleteChecklistItem,
};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
