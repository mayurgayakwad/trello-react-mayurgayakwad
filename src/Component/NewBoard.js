import React, { Component } from "react";
import styled from "@emotion/styled";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import CircularProgress from "@mui/material/CircularProgress";
import { Paper, Button, Dialog, DialogTitle } from "@mui/material";
import { Stack, DialogContent, TextField, DialogActions } from "@mui/material";
import { getList, createList, deleteList } from "./Redux/store";
import Navbar from "./Navbar";
import Cards from "./Cards";
import { connect } from "react-redux";

class NewBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allList: [],
      open: false,
      listName: "",
    };
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
    });
  };
  newListName = (event) => {
    this.setState({
      listName: event.target.value,
    });
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getList(id);
  }
  render() {
    const { id } = this.props.match.params;
    // console.log(this.props.store.allList);
    return (
      <>
        <Navbar />
        <div className="h-[100vh] flex bg-gradient-to-r from-purple-500 to-pink-500">
          <div className="overflow-auto flex">
            <div className="h-fit flex">
              {this.props.store.Lists ? (
                this.props.store.Lists.map((list) => (
                  <div
                    key={list.id}
                    className="w-[300px] h-[100%] ml-6 shadow-md bg-slate-100 rounded-sm p-4"
                  >
                    <p key="p" className="text-lg font-semibold ">
                      {list.name}
                      <DeleteForeverIcon
                        className="hover:text-red-600"
                        style={{
                          cursor: "pointer",
                          marginRight: "4px",
                          float: "right",
                        }}
                        onClick={(id) => {
                          this.props.deleteList(list.id);
                        }}
                      ></DeleteForeverIcon>
                    </p>
                    <hr />
                    <div className="snap-x mt-2">
                      <Cards listid={list.id}></Cards>
                    </div>
                  </div>
                ))
              ) : (
                <CircularProgress style={{ margin: 10 }} />
              )}
            </div>
          </div>
          <Button
            variant="outlined"
            onClick={this.handleClickOpen}
            sx={{
              height: "40px",
              width: "300px",
              marginLeft: "24px",
              marginTop: "10px",
              marginRight: "24px",
              color: "white",
              border: "solid 1px white",
            }}
          >
            + Add Another List
          </Button>
          <Stack>
            <Dialog open={this.state.open} onClose={this.handleClose}>
              <DialogTitle>Add a list</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Enter list title…"
                  type="text"
                  fullWidth
                  variant="outlined"
                  onChange={this.newListName}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button
                  onClick={() => {
                    this.props.createList(id, this.state.listName);
                    this.handleClose();
                    this.setState({ listName: "" });
                  }}
                >
                  Create
                </Button>
              </DialogActions>
            </Dialog>
          </Stack>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    store: state,
  };
};

const mapDispatchToProps = {
  getList: getList,
  createList: createList,
  deleteList: deleteList,
};
export default connect(mapStateToProps, mapDispatchToProps)(NewBoard);
