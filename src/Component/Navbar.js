import { AppBar, Toolbar, CardMedia, Search } from "@mui/material";
import React, { Component } from "react";
import AppsIcon from "@mui/icons-material/Apps";
import SearchIcon from "@mui/icons-material/Search";
import SvgIcon from "@mui/material/SvgIcon";
import { Link } from "react-router-dom";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <AppBar className="h-12 flex justify-center" position="static">
          <Toolbar>
            <AppsIcon />
            <Link to={"/"}>
              {" "}
              <CardMedia
                component="img"
                flexgrow={1}
                alt="logo not found"
                height=""
                sx={{ width: 100, marginLeft: 2 }}
                image="https://a.trellocdn.com/prgb/dist/images/header-logo-spirit.d947df93bc055849898e.gif"
              />
            </Link>
          </Toolbar>
        </AppBar>
      </>
    );
  }
}

export default Navbar;
