import React, { Component } from "react";
import { Box, Container, Grid, Typography } from "@mui/material";
import DashboardIcon from "@mui/icons-material/Dashboard";

import YourWorkSpace from "./YourWorkSpace";
class HeroSection extends Component {
  render() {
    return (
      <Container sx={{ marginTop: 4 }}>
        <Box>
          <Grid container>
            <Grid xs={3}>
              <div className="lg:flex py-2 w-[250px] px-4 border rounded-lg hidden">
                <Box>
                  <Typography fontWeight="bold" fontSize={14}>
                    <DashboardIcon /> Boards
                  </Typography>
                </Box>
              </div>
            </Grid>
            <Grid xs={9}>
              <div className="flex">
                <YourWorkSpace />
              </div>
            </Grid>
          </Grid>
        </Box>
      </Container>
    );
  }
}

export default HeroSection;
