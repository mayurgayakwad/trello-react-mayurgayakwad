import { act } from "react-dom/test-utils";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

const key = "ba0effa96c13b536f613cc58d7c7dad9";
const token = `ATTAb83a3218b7c8df820b9721f3d9a6b3227907f128eb8e5604a6704c7c4aa2eefcC8E6FC13`;

const initialstate = {
  listOfBoards: [],
  Lists: [],
  Cards: [],
  checkList: [],
};

function gettingBoards(data) {
  return {
    type: "GETTING_BOARDS",
    payload: data,
  };
}
function creatingBoards(data) {
  return {
    type: "CREATING_BOARDS",
    payload: data,
  };
}
function gettingList(data) {
  return {
    type: "GETTING_LISTS",
    playlaod: data,
  };
}

function creatingList(data) {
  return {
    type: "CREATING_LIST",
    playlaod: data,
  };
}
function deletingList(data) {
  return {
    type: "DELETING_LIST",
    playlaod: data,
  };
}
function gettingCards(data) {
  return {
    type: "GETTING_CARDS",
    playlaod: data,
  };
}
function creatingCards(data) {
  return {
    type: "CREATING_CARDS",
    playlaod: data,
  };
}
function deletingCards(data) {
  return {
    type: "DELETING_CARD",
    playlaod: data,
  };
}
function gettingChecklist(data) {
  return {
    type: "GETTING_CHECKLIST",
    playlaod: data,
  };
}

function creatingChecklist(data) {
  return {
    type: "CREATING_CHECKLIST",
    playlaod: data,
  };
}
function deletingChecklist(data) {
  return {
    type: "DELETING_CHECKLIST",
    playlaod: data,
  };
}
function creatingNewCheckItems(data) {
  return {
    type: "CREATING_CHECKITEM",
    playlaod: data,
  };
}

function deletingChecklistItem(data) {
  return {
    type: "DELETING_CHECKLISTITEM",
    playlaod: data,
  };
}
export function getBoards() {
  return function (dispatch) {
    dispatch(gettingBoards(null));
    fetch(
      `https://api.trello.com/1/members/me/boards?fields=name,url&key=${key}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(gettingBoards(JSON.parse(text)));
      })
      .catch((err) => console.error(err));
  };
}
export function createBoards(input) {
  return function (dispatch) {
    // dispatch(creatingBoards(null));
    fetch(
      `https://api.trello.com/1/boards/?name=${input}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(creatingBoards(text));
      })
      .catch((err) => console.error(err));
  };
}

export function getList(ids) {
  return function (dispatch) {
    // dispatch(gettingList(null));
    fetch(
      `https://api.trello.com/1/boards/${ids}/lists?key=${key}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(gettingList(JSON.parse(text)));
      })
      .catch((err) => console.error(err));
  };
}

export function createList(id, listName) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${id}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(creatingList(text));
      })
      .catch((err) => console.error(err));
  };
}

export function getCards(id) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(gettingCards(JSON.parse(text)));
      })
      .catch((err) => console.error(err));
  };
}
export function createCards(listId, newCardName) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/cards?idList=${listId}&name=${newCardName}&key=${key}&token=${token}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(creatingCards(text));
      })
      .catch((err) => console.error(err));
  };
}

export function getChecklist(cardsID) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/cards/${cardsID}/checklists?key=${key}&token=${token}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(gettingChecklist(JSON.parse(text)));
      })
      .catch((err) => console.error(err));
  };
}
export function deleteList(id) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(deletingList(id));
      })
      .catch((err) => console.error(err));
  };
}
export function deleteCard(id) {
  return function (dispatch) {
    fetch(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`, {
      method: "DELETE",
    })
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(deletingCards(id));
      })
      .catch((err) => console.error(err));
  };
}
export function createChecklist(createChecklistName, cardsID) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/checklists?name=${createChecklistName}&idCard=${cardsID}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => response.text())
      .then((text) => {
        dispatch(creatingChecklist(text));
      })
      .catch((err) => console.error(err));
  };
}
export function deleteChecklist(id) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`,
      {
        method: "DELETE",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(deletingChecklist(id));
      })
      .catch((err) => console.error(err));
  };
}

export function createsNewCheckItems(ChecklistID, Name) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/checklists/${ChecklistID}/checkItems?name=${Name}&key=${key}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(creatingChecklist(text));
      })
      .catch((err) => console.error(err));
  };
}

export function deleteChecklistItem(ChecklistID, checklistItemId) {
  return function (dispatch) {
    fetch(
      `https://api.trello.com/1/checklists/${ChecklistID}/checkItems/${checklistItemId}?key=${key}&token=${token}`,
      {
        method: "DELETE",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => {
        dispatch(deletingChecklistItem(checklistItemId));
      })
      .catch((err) => console.error(err));
  };
}
function reducer(state = initialstate, action) {
  switch (action.type) {
    case "GETTING_BOARDS":
      return {
        ...state,
        listOfBoards: action.payload,
      };
    case "CREATING_BOARDS":
      return {
        ...state,
        listOfBoards: [...state.listOfBoards, JSON.parse(action.payload)],
      };
    case "GETTING_LISTS":
      return {
        ...state,
        Lists: action.playlaod,
        Cards: [],
      };
    case "CREATING_LIST":
      return {
        ...state,
        Lists: [...state.Lists, JSON.parse(action.playlaod)],
      };
    case "DELETING_LIST":
      const newLists = state.Lists.filter((dellist) => {
        return dellist.id !== action.playlaod;
      });
      return {
        ...state,
        Lists: [...newLists],
      };
    case "GETTING_CARDS":
      return {
        ...state,
        Cards: [...state.Cards, ...action.playlaod],
      };
    case "CREATING_CARDS":
      return {
        ...state,
        Cards: [...state.Cards, JSON.parse(action.playlaod)],
      };
    case "DELETING_CARD":
      const newCrads = state.Cards.filter((delcard) => {
        return delcard.id !== action.playlaod;
      });
      return {
        ...state,
        Cards: [...newCrads],
      };
    case "GETTING_CHECKLIST":
      return {
        ...state,
        checkList: [...action.playlaod],
      };
    case "CREATING_CHECKLIST":
      return {
        ...state,
        checkList: [...state.checkList, JSON.parse(action.playlaod)],
      };
    case "DELETING_CHECKLIST":
      const newChecklist = state.checkList.filter((delchecklist) => {
        return delchecklist.id !== action.playlaod;
      });
      return {
        ...state,
        checkList: [...newChecklist],
      };
    case "CREATING_CHECKITEM":
      return {
        ...state,
        checkList: [...state.checkList, action.playlaod],
      };
    case "DELETING_CHECKLISTITEM":
      const newChecklistItem = state.checkList.filter((delchecklistItem) => {
        return delchecklistItem.id !== action.playlaod;
      });
      return {
        ...state,
        checkList: [...newChecklistItem],
      };
    default:
      return state;
  }
}

const store = createStore(reducer, applyMiddleware(thunk));
store.subscribe(() => {
  console.log(store.getState());
});

export default store;
