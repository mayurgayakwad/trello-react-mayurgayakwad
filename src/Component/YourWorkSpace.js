import { Typography, CardMedia, OutlinedInput } from "@mui/material";
import { Box } from "@mui/system";
import React, { Component } from "react";
import Menu from "@mui/material/Menu";
import CircularProgress from "@mui/material/CircularProgress";
import MenuItem from "@mui/material/MenuItem";
import CloseIcon from "@mui/icons-material/Close";
import { getBoards, createBoards } from "./Redux/store";

import { Link } from "react-router-dom";
import { connect } from "react-redux";

class YourWorkSpace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      input: "",
    };
  }

  handleClick = (event) => {
    this.setState({
      open: true,
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
    });
  };
  handleInput = (event) => {
    this.setState({
      input: event.target.value,
    });
  };

  componentDidMount() {
    this.props.getBoards();
  }
  render() {
    return (
      <>
        <Box>
          <Typography fontWeight="bold" color={"#5e6c84"} marginY="25px">
            YOUR WORKSPACES
          </Typography>
          <div className="flex gap-2 items-center mb-10">
            <Typography
              border={1}
              textAlign="center"
              color={"white"}
              fontSize={"20px"}
              bgcolor={"#00875a"}
              fontWeight="bold"
              width={40}
            >
              T
            </Typography>
            <Typography fontWeight={700}>Trello Workspace</Typography>
          </div>
          <div className="flex gap-2 flex-wrap">
            {this.props.store.listOfBoards ? (
              this.props.store.listOfBoards.map((boards, index) => (
                <Link key={`L${index}`} to={`${boards.id}`}>
                  <div
                    key={`d${index}`}
                    className="border w-[200px] h-24 bg-blue-500 rounded-[5px] text-white text-lg font-bold "
                  >
                    <p key={`p${index}`} className="px-4">
                      {boards.name}
                    </p>
                  </div>
                </Link>
              ))
            ) : (
              <CircularProgress />
            )}

            <button
              variant="outlined"
              className="border w-[200px] rounded-[5px] h-24 bg-[#091e420a] text-sm"
              onClick={this.handleClick}
            >
              Create new board
            </button>
          </div>
          <div className="px-10">
            <Menu
              id="basic-menu"
              open={this.state.open}
              // onClose={this.state.handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
              style={{
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",
                left: "50%",
                transform: "translate(-10%, 0)",
              }}
            >
              <div className="flex w-[325px] justify-between">
                <Typography paddingLeft={15}>Create board</Typography>{" "}
                <MenuItem>
                  {" "}
                  <CloseIcon
                    onClick={this.handleClose}
                    className="h-2 float-right pb-2"
                  />
                </MenuItem>
              </div>
              <hr />
              <CardMedia
                component="img"
                alt="logo not found"
                height=""
                sx={{ paddingX: 8, marginBottom: 2 }}
                image="https://a.trellocdn.com/prgb/dist/images/board-preview-skeleton.14cda5dc635d1f13bc48.svg"
              />
              <Typography paddingX={2}>
                Board title
                <span className="text-[#eb5a46] text-2xl ml-1">*</span>
              </Typography>

              <OutlinedInput
                onChange={this.handleInput}
                className="w-[90%] ml-4 h-10 text-xl"
              />
              {this.state.input.length > 0 ? (
                ""
              ) : (
                <Typography className="pl-4 pt-2">
                  👋 Board title is required
                </Typography>
              )}

              {this.state.input.length > 0 ? (
                <button
                  onClick={() => {
                    this.props.createBoards(this.state.input);
                    this.handleClose();
                    this.setState({ input: "" });
                  }}
                  className="w-[90%] border ml-4 my-4 bg-[#0079bf] rounded-sm text-white h-10 text-lg"
                >
                  Create
                </button>
              ) : (
                <button className="w-[90%] border ml-4 my-2 cursor-not-allowed rounded-sm bg-slate-100 h-10 text-lg">
                  Create
                </button>
              )}
            </Menu>
          </div>
        </Box>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    store: state,
  };
};

const mapDispatchToProps = {
  getBoards: getBoards,
  createBoards: createBoards,
};

export default connect(mapStateToProps, mapDispatchToProps)(YourWorkSpace);
