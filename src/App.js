import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
// import { Route } from "router";
import "./App.css";
import HeroSection from "./Component/HeroSection";
import Navbar from "./Component/Navbar";
import NewBoard from "./Component/NewBoard";
import YourWorkSpace from "./Component/YourWorkSpace";
import HomePage from "./HomePage";

function App() {
  return (
    <>
      <BrowserRouter>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/:id" exact component={NewBoard}></Route>
      </BrowserRouter>
    </>
  );
}

export default App;
